package com.jaber.restfulwebservice.controller;

import com.jaber.restfulwebservice.bean.HelloWorldBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jaber
 * @Date 9/3/2022
 * @Time 9:05 PM
 * @Project restful-web-service
 */
@RestController
public class HelloWorldController {

    @GetMapping("/hello-world")
    public String helloWorld() {
        return "Hello World";
    }

    @GetMapping("/hello-world-bean")
    public HelloWorldBean helloWorldBean() {
        return new HelloWorldBean("Hello World");
    }

    @GetMapping("/hello-world/path-variable/{name}")
    public HelloWorldBean HelloWorldPathVariable(@PathVariable("name") String name) {
        return new HelloWorldBean(String.format("Hello World, %s", name));
    }
}
