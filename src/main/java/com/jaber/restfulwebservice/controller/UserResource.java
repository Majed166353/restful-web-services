package com.jaber.restfulwebservice.controller;

import com.jaber.restfulwebservice.bean.User;
import com.jaber.restfulwebservice.exception.UserNotFoundException;
import com.jaber.restfulwebservice.service.UserDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

/**
 * @author Jaber
 * @Date 9/3/2022
 * @Time 9:51 PM
 * @Project restful-web-service
 */
@RestController
public class UserResource {
    @Autowired
    private UserDaoService userDaoService;

    @Autowired
    private MessageSource messageSource;
    @GetMapping("/users")
    public List<User> retrieveAllUsers() {
        return userDaoService.findAll();
    }

    //HATEOAS = Hypermedia as the engine of application state
    @GetMapping("/users/{id}")
    public EntityModel<User> retrieveAUser(@PathVariable("id") int id) throws NoSuchMethodException {
        User user = userDaoService.findOne(id);
        if (user == null) {
            throw new UserNotFoundException("id-" + id);
        }

        //Hateoas concept >> One way of doing this
//        EntityModel<User> resource = EntityModel.of(user);
//        Method method = UserResource.class.getMethod("retrieveAllUsers");
//        Link link = linkTo(method).withRel("all-users");
//        resource.add(link);

        //Hateoas concept >> Another way of doing this
        EntityModel<User> resource = EntityModel.of(user);
        resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).retrieveAllUsers()).withRel("all-users"));
        return resource;
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
        User savedUser = userDaoService.save(user);
        //user/{id}
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedUser.getId()).toUri(); // this basically get the current request uri and append the id. /user/3
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/users/{id}")
    public void deleteById(@PathVariable("id") int id) {
        User user = userDaoService.deleteById(id);
        if (user == null) {
            throw new UserNotFoundException("id " + id);
        }
    }

    @GetMapping("/hello-world-internationalized")
    public String goodMorningInternationalized() {
        return messageSource.getMessage("good.morning.message", null, LocaleContextHolder.getLocale());
    }
}
