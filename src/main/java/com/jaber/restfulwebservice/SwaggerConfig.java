package com.jaber.restfulwebservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
public class SwaggerConfig {
    public static final Contact DEFAULT_CONTACT = new Contact("ABDULLA AL JABER", "http://www.majed-2020.heroku.com", "Majedabdullah635@gmail.com");
    public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("My Awesome Restful Web Services", "Restful Web Services API Documentation", "1.0", "urn:tos", DEFAULT_CONTACT, "Apache 2.0", "http://www.majed-2020.heroku.com", new ArrayList());
    private static final Set<String> DEFAULT_PRODUCE_AND_CONSUMES = new HashSet<String>(Arrays.asList("application/json", "application/xml"));

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_PRODUCE_AND_CONSUMES)
                .consumes(DEFAULT_PRODUCE_AND_CONSUMES);
    }
}
