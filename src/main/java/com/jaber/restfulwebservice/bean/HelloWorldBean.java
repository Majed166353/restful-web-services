package com.jaber.restfulwebservice.bean;

/**
 * @author Jaber
 * @Date 9/3/2022
 * @Time 9:07 PM
 * @Project restful-web-service
 */
public class HelloWorldBean {
    private String message;

    public HelloWorldBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
