package com.jaber.restfulwebservice.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Jaber
 * @Date 9/3/2022
 * @Time 9:31 PM
 * @Project restful-web-service
 */
@ApiModel(description = "Take a look at the validations carefully")
public class User {
    private Integer id;
    @Size(min = 2, message = "Message should at-least be 2 chars!")
    private String name;
    @Past
    @ApiModelProperty(notes = "Birthdate should be in the past")
    private Date birthDate;

    public User(Integer id, String name, Date birthDate) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
