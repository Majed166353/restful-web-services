package com.jaber.restfulwebservice.service;

import com.jaber.restfulwebservice.bean.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author Jaber
 * @Date 9/3/2022
 * @Time 9:35 PM
 * @Project restful-web-service
 */
@Component
public class UserDaoService {
    private static List<User> users = new ArrayList<>();
    private static int userCount = 4;

    static {
        users.add(new User(1, "Adam", new Date()));
        users.add(new User(2, "Jaber", new Date()));
        users.add(new User(3, "Femina", new Date()));
        users.add(new User(4, "Sami", new Date()));
    }

    public List<User> findAll() {
        return users;
    }

    public User save(User user) {
        if (user.getId() == null) {
            user.setId(++userCount);
        }
        users.add(user);
        return user;
    }

    public User findOne(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public User deleteById(int id) {
        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {
            User user = iterator.next();
            if (user.getId() == id) {
                iterator.remove();
                return user;
            }
        }
        return null;
    }

}
